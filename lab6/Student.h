//
// Created by Zippo Xie on 2/19/18.
//

#ifndef LAB6_STUDENT_H
#define LAB6_STUDENT_H
#include <iostream>
#include <string>
using namespace std;

class Student {
public:
    Student();
    Student(Student& copy);
    Student(Student&& move);
    Student(string first, string last, int age);
    ~Student() {cout << "~Student(" << firstName << ") called" << endl;}
    void swap(Student& another) noexcept;
    Student&operator=(Student b) {b.swap(*this); return (*this);}

    friend ostream &operator<<(ostream &output, const Student &A) {
        output << "Firstname: " << A.firstName << endl;
        output << "Lastname: " << A.lastName << endl;
        output << "Age: " << A.age << endl;
        return output;
    }

    string GetFirst(){return firstName;}
    bool SetFirst(string newFirst);
    string GetLast(){return lastName;}
    bool SetLast(string newLast);
    int Getage() { return age;}
    bool Setage(int newage);
private:
    string firstName;
    string lastName;
    int age;
};
#endif //LAB6_STUDENT_H
