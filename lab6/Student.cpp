//
// Created by Zippo Xie on 2/19/18.
//

#include "Student.h"

Student::Student() {
    cout << "Student() called" << endl;
    firstName = "Default";
    lastName = "Name";
    age = 1;
}
Student::Student(Student& copy) {
    cout << "Student(Student const& copy) called" << endl;
    firstName = copy.GetFirst();
    lastName = copy.GetLast();
    age = copy.Getage();
}
Student::Student(Student&& move) : Student::Student() {
    cout << "Student(Student&& move) called" << endl;
    move.swap(*this);
}
Student::Student(string first, string last, int age) {
    cout << "Student(string first, string last, int age) called" << endl;
    firstName = first;
    lastName = last;
    this->age = age > 0 ? age : 1;
}
void Student::swap(Student& another) noexcept {
    string this_first = firstName;
    string this_last = lastName;
    int this_age = age;

    firstName = another.GetFirst();
    lastName = another.GetLast();
    age = another.Getage();
    if(!another.Setage(this_age)) {another.Setage(1);}
    if(!another.SetFirst(this_first)) {another.SetFirst("Default");}
    if(!another.SetLast(this_last)) {another.SetLast("Name");}
}

bool Student::SetFirst(string newFirst) {
    firstName = newFirst;
    return true;
}

bool Student::SetLast(string newLast) {
    lastName = newLast;
    return true;
}

bool Student::Setage(int newage) {
    if(newage > 0) {
        age = newage;
        return true;
    }
    return false;
}