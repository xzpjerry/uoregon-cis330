#include "Student.h"
#include <vector>
#include <memory>
int main() {
    Student A;
    cout << A;

    cout << endl;
    Student B("Jerry", "Belson", 23);
    cout << B;

    cout << endl;
    Student CopyA(A);
    cout << CopyA;

    cout << endl;
    Student C = B;
    cout << C;

    cout << endl;
    unique_ptr<Student[]> test(new Student[3]);
    test[0] = A;
    test[1] = B;
    test[2] = C;

    return 0;
}