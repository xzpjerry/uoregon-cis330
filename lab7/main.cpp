#include <iostream>
#include <vector>
#include <random>
#include <algorithm>
using namespace std;

void print(vector<int> &v) {
    for(vector<int>::iterator i = v.begin(); i != v.end(); i++) {
        cout << *i << " ";
    }
    cout << endl;
}

int main() {
    vector<int> v;
    int i = 15;
    random_device rd;
    mt19937 mt(rd());
    uniform_int_distribution<> dist(1, 100);

    while(i--) {
        v.push_back(dist(mt));
    }
    print(v);
    sort(v.begin(), v.end());
    print(v);

    vector<int> vCopy;
    copy(v.begin(),v.end(),back_inserter(vCopy));
    print(vCopy);
    random_shuffle(vCopy.begin(), vCopy.end());
    print(vCopy);
    return 0;
}