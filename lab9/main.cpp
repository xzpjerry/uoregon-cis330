#include <iostream>
#include <numeric>
#include <fstream>
#include <string>
using namespace std;

int main() {
    int arr[10];
    srand((unsigned)time(0));
    for(int& i : arr) {
        i = (rand() % 5);
        cout << i << " ";
    }
    cout << endl;

    pid_t child = fork();
    if(child != 0) {
        cout << "I am child\n";

        int tmp_sum = accumulate(arr, arr + 4, 0);
        ofstream ofile;
        ofile.open("output.txt");
        if(ofile.is_open()) {
            ofile << tmp_sum << endl;
            ofile.close();
            cout << "Done\n";
        }
    } else {
        cout << "I am parent\n";

        int child_status;
        wait(&child_status);
        ofstream ofile;
        int tmp_sum = accumulate(arr+4, arr + 10, 0);
        ofile.open("output.txt", ios::app);
        if(ofile.is_open()) {
            ofile << tmp_sum << endl;
            ofile.close();
            cout << "Done\n";
        }

        ifstream ifile;
        ifile.open("output.txt");
        if(ifile.is_open()) {
            int sum = 0;
            while(!ifile.eof()) {
                string line;
                getline(ifile, line);
                sum += atoi(line.c_str());
            }
            cout << sum;
        }


    }



    return 0;
}