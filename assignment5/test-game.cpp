#include "game.hpp"

int main() {
	int wid, hei, steps;
	wid=hei=steps=-1;
	cout << "Please enter the size of the grid (int int):";
	while(!(cin >> wid >> hei)) {
        cout << "Please enter the size of the grid (int int):";
		cin.clear();
        cin.ignore(numeric_limits<streamsize>::max(), '\n');
	}
	cout << "Please enter the number of steps (int):";
	while(!(cin >> steps)) {
        cout << "Please enter the number of steps (int):";
		cin.clear();
        cin.ignore(numeric_limits<streamsize>::max(), '\n');
	}
    map test(wid, hei);
    test.print();
    test.steps(steps);
    return 0;
}