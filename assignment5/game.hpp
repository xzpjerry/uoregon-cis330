#include <iostream>
#include <random>
#include <vector>
#include <string.h> // I forgot adding this
#include <tuple>
using namespace std;
static char group[4] = {'S','W','F','.'};

struct node {
    char status; // One in {'S', 'W', "F", '.'}
    int neighbours = 0; // counter of non-empty neighbours
    int list[4] = {0, 0, 0, 0};  // using a list to keep track species around this node
    // list[0] = num_of_S; [1] = num_of_W; [2] = numOfFarmer; 
    //[3] = numOfEmpty
};

class map {
public:
    map(int width, int height);
    ~map();
    void destory_neighbor();
    void init_or_reset();

    void update_nodes(int i, int j);
    bool over_populated(char status, int i, int j);
    int reproducible(char status, int i, int j);
    void steps(int rounds);
    void step();
    void print();
private:
    void gen_map();
    void alloc_nodes();
    void categorize(char status, int i, int j);
    void farmer_wanna_move(int i, int j);
    node **neighbour_map = NULL;
    int width, height;
};
