#include "game.hpp"

map::map(int width, int height) {
    this->height = height;
    this->width = width;
    init_or_reset();
}
map::~map(){
    destory_neighbor();
}

void map::destory_neighbor() {
    for(int i = height - 1; i != -1; i--) {
        delete neighbour_map[i];
    }
    delete neighbour_map;
}
void map::init_or_reset() {
    if(neighbour_map == NULL){ // init
        alloc_nodes();
        gen_map();
    } else {  //reset
        for(int i = height - 1; i != -1; i--) {
            for(int j = width - 1; j != -1; j--) {
                neighbour_map[i][j].neighbours = 0;
                memset(neighbour_map[i][j].list, 0, 4 * sizeof(int));
            }
        }
    }
    for(int i = height - 1; i != -1; i--) {
        for(int j = width - 1; j != -1; j--) {
            update_nodes(i, j);
        }
    }
}

void map::update_nodes(int i, int j) {

    if(i != 0) { // upper side
        categorize(neighbour_map[i - 1][j].status, i, j);
        if(j != 0) {
            categorize(neighbour_map[i - 1][j - 1].status, i, j);
        }
        if(j != width - 1) {
            categorize(neighbour_map[i - 1][j + 1].status, i, j);
        }
    }

    if(i != height - 1) { // lower side
        categorize(neighbour_map[i + 1][j].status, i, j);
        if(j != 0) {
            categorize(neighbour_map[i + 1][j - 1].status, i, j);
        }
        if(j != width - 1) {
            categorize(neighbour_map[i + 1][j + 1].status, i, j);
        }
    }

    if(j != 0) { // left
        categorize(neighbour_map[i][j - 1].status, i, j);
    }
    if(j != width - 1) { // right
        categorize(neighbour_map[i][j + 1].status, i, j);
    }

}

bool map::over_populated(char status, int i, int j) {
    switch(status) {
        case 'S': {
            return neighbour_map[i][j].list[0] >= 3;
        }
        case 'W': {
            return neighbour_map[i][j].list[1] >= 3;
        }
        case 'F': {
            return neighbour_map[i][j].list[2] >= 3;
        }
        default: return false;
    }
}

int map::reproducible(char status, int i, int j) {
    
    if(neighbour_map[i][j].neighbours >= 2){
        for(int b = 0; b != 3; b++) {
            if(neighbour_map[i][j].list[b] != 2) continue;
            else return b;
        }
    }
    return 3;
    
}

void map::steps(int rounds) {
    for(int i = 0; i != rounds; i++) {
        cout << "Step " << i + 1 << ":" << endl;
        init_or_reset();
        step();
        print();
    }
}

void map::step() {
    for(int i = 0; i != height; i++) {
        for (int j = 0; j != width; j++) {
            node *now = &(neighbour_map[i][j]);
            if((*now).neighbours >= 3 && over_populated((*now).status, i, j)) (*now).status = '.'; // excessive
            if((*now).status == 'S' && (*now).list[1] > 0) (*now).status = '.'; // Wolves vs sheeps
            if((*now).status == '.') (*now).status = group[reproducible((*now).status, i, j)]; // 1+1=2
            if((*now).status == 'W' && (*now).list[2] > 0) (*now).status = '.'; // Farmers vs wolves
            if((*now).status == 'W' && (*now).list[0] == 0) (*now).status = '.'; // Wolves with no food
            if((*now).status == 'F' && (*now).list[3] > 0) farmer_wanna_move(i, j);
        }
    }
}

void map::print(){
    for(int i = 0; i != height; i++) {
        for(int j = 0; j != width; j++) {
            cout << neighbour_map[i][j].status << ' ';
        }
        cout << endl;
    }
    cout << endl;
    // cout << endl;
    //    cout << endl;
    //    for(int i = 0; i != height; i++) {
    //        for(int j = 0; j != width; j++) {
    //            cout << neighbour_map[i][j].neighbours << ' ';
    //        }
    //        cout << endl;
    //    }
    //    for(int i = 0; i != height; i++) {
    //        for(int j = 0; j != width; j++) {
    //            cout << "(" << i << "," << " " << j << ")'s neighbour: ";
    //            for(int b = 0; b != 4; b++)
    //                cout << neighbour_map[i][j].list[b] << ' ';
    //            cout << endl;
    //        }
    //        cout << endl;
    //    }
}

void map::gen_map() {

    random_device rd;
    mt19937 mt(rd());
    uniform_int_distribution<> dist(0, 3);

    for(int i = height - 1; i != -1; i--) {
        for(int j = width - 1; j != -1; j--) {
            neighbour_map[i][j].status = group[dist(mt)];
        }
    }
}
void map::alloc_nodes() {
    neighbour_map = new node*[height];
    for(int i = height - 1; i != -1; i--) {
        neighbour_map[i] = new node[width];
    }
}

void map::categorize(char status, int i, int j) {
    switch(status) {
        case 'S': {
            neighbour_map[i][j].list[0]++; 
            neighbour_map[i][j].neighbours++; 
            break;}
        case 'W': {
            neighbour_map[i][j].list[1]++; 
            neighbour_map[i][j].neighbours++; 
            break;}
        case 'F': {
            neighbour_map[i][j].list[2]++; 
            neighbour_map[i][j].neighbours++; 
            break;}
        case '.': {
            neighbour_map[i][j].list[3]++; 
            break;}
        default: {cout << "Fatal error, illegal input!!!!"; exit(1);}
    }
}

void map::farmer_wanna_move(int i, int j) {
    int irange_up = (0 > (i - 1)) ? 0 : (i-1); // max(0, i-1)
    int irange_down = ((i + 1) > height - 1) ? (height-1) : (i + 1); // max(i+1, height -1)
    int jrange_left = (0 > (j - 1)) ? 0 : (j-1); // max(0, j-1)
    int jrange_right = ((j + 1) >= width - 1) ? (width-1) : (j + 1); // max(j+1, width - 1)

    random_device rd;
    mt19937 mt(rd());
    uniform_int_distribution<> dist_i(irange_up, irange_down);
    uniform_int_distribution<> dist_j(jrange_left, jrange_right);
    int new_i, new_j;
    do {
        new_i = dist_i(mt);
        new_j = dist_j(mt);
    } while(neighbour_map[new_i][new_j].status != '.');
    neighbour_map[i][j].status = '.';
    neighbour_map[new_i][new_j].status = 'F';
}


