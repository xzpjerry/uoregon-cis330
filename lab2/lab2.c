#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#define ROW 3
#define COL 4

bool arrayEqual(int a[ROW][COL], int b[ROW][COL], int m, int n)
{
    for(int i = m - 1; i != -1; i--) {
        for(int j = n - 1; j != -1; j--) {    
            if(a[i][j] != b[i][j]) {
                return false;
            }
        }
    }
    return true;
}
void print_2d_dy(int **a, int m, int n) {
    for(int i = m - 1; i != -1; i--) {
        for(int j = n - 1; j != -1; j--) {    
            printf("%d ", a[i][j]);
        }
    }
}
bool arrayEqual_dy(int ***a, int ***b, int m, int n)
{
    for(int i = m - 1; i != -1; i--) {
        for(int j = n - 1; j != -1; j--) {    
            if((*a)[i][j] != (*b)[i][j]) {
                return false;
            }
        }
    }
    return true;
}
void init(int ***v, int r, int c) {
    *v = (int **)malloc(r * sizeof(int *));

    for(int i = r - 1; i != -1; i--) {
        int size = c * sizeof(int);
        (*v)[i] = malloc(size);
        memset((*v)[i], 0, size);
    }
}

int main() {
    int **a_dy;
    int **b_dy;
    init(&a_dy, ROW, COL);
    init(&b_dy, ROW, COL);
    printf(arrayEqual_dy(&a_dy, &b_dy, ROW, COL) ? "true" : "false");
    puts("");
    b_dy[0][0] = 1;
    printf(arrayEqual_dy(&a_dy, &b_dy, ROW, COL) ? "true" : "false");
    puts("");
    print_2d_dy(b_dy, ROW, COL);


    int a[ROW][COL] = {
        {0, 1, 2, 3} ,
        {4, 5, 6, 7} ,
        {8, 9, 10, 11}
    };

    int b[ROW][COL] = {
        {0, 1, 2, 3} ,
        {4, -1, 6, 7} ,
        {8, 9, 10, 11}   
    };
    int c[ROW][COL] = {
        {0, 1, 2, 3} ,
        {4, 5, 6, 7} ,
        {8, 9, 10, 11}  
    };
    printf(arrayEqual(a, b, ROW, COL) ? "true" : "false");
    puts("");
    printf(arrayEqual(a, c, ROW, COL) ? "true" : "false");
    return 0;
}