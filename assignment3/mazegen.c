#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
const char FREE_PATH = '.';
const char WALL = '@';

// It still has a bug, I am trying to figure it out
// It seems like there is a range issue with the array
// It works most of the time
void destory_2d_arr(char ***two_d_arr, int size) {
    for(int i = size - 1; i != -1; i--) {
        free((*two_d_arr)[i]);
    }
    free((*two_d_arr));
}
void init(char ***maze, char ***critical_nodes, int size) {
	(*maze) = (char **)malloc(size * sizeof(char *));
	(*critical_nodes) = (char **)malloc(size * sizeof(char *));
	int size_minus1 = size - 1;
	for(int i = size_minus1; i != -1; i--) {
		(*maze)[i] = malloc(size);
		(*critical_nodes)[i] = malloc(size);
		memset((*critical_nodes)[i], '0', size);
		memset((*maze)[i], FREE_PATH, size);
		(*maze)[i][0] = WALL;
		(*maze)[i][size_minus1] = WALL;
	}
	memset((*maze)[0], WALL, size);
	memset((*maze)[size_minus1], WALL, size);

}
void print_maze(char **maze, int size, FILE *fp) {
	for(int i = 0; i != size; i++) {
		for(int j = 0; j != size; j++) {
			printf("%c ", maze[i][j]);
			if(fp != NULL) {
				fprintf(fp, "%c ", maze[i][j]);
			}
		}
		puts("");
		if(fp != NULL) {
			fprintf(fp, "\n");
		}
	}
}
int* available(char **maze, int mazeSize_minus1, int cx, int cy) {
	// a helper function using to check whether the exit and entry have been blocked
    int counter = 0;
    int status_collector = 0;
    static int result[2];

    if(cy < mazeSize_minus1) { // right
    	if(maze[cx][cy + 1] == FREE_PATH) {counter++;}
    	if(maze[cx][cy + 1] == WALL) {status_collector++;} // 0b1
    }

    if(cy > 0) { // left
    	if(maze[cx][cy - 1] == FREE_PATH) {counter++;}
    	if(maze[cx][cy - 1] == WALL) {status_collector += 2;} //0b10
    }

    if(cx > 0) { // up
    	if(maze[cx - 1][cy] == FREE_PATH) {counter++;}
    	if(maze[cx - 1][cy] == WALL) {status_collector += 4;} // 0b100
    }

	if(cy < mazeSize_minus1) { // down
    	if(maze[cx + 1][cy] == FREE_PATH) {counter++;}
    	if(maze[cx + 1][cy] == WALL) {status_collector += 8;} // 0b1000
    }
    result[0] = counter;
    result[1] = status_collector;
    
    return result;
}

void recursive(char ***maze, char ***critical_nodes, int size, int left_most_row_index, int left_most_col_index) {
	//Recursive division method
	//Begin with the maze's space with no walls in the middle. 
	//Divide it with a randomly positioned wall where each wall contains a randomly positioned passage(the critical nodes) opening within it. 
	//Then recursively repeat the process on the subchambers until all chambers are minimum sized.
	// By avoiding blocking the critical nodes, each area of the maze must be viable(at least will have one path going through them)
	if(size < 2) {
		return;
	}
	
	int next_size = size / 2;
	int crow = left_most_row_index;
	int ccol = left_most_col_index;
	
	for(int i = size ; i != 0; i--) {
		if((*critical_nodes)[next_size + left_most_row_index][ccol++] == '0'){(*maze)[next_size + left_most_row_index][ccol] = WALL;}
		if((*critical_nodes)[crow][next_size + left_most_col_index] == '0') {(*maze)[crow][next_size + left_most_col_index] = WALL;}
	}

	// passages of the vertical wall, one above the horizontal wall, one below the horizontal wall
	int col_breach1 = rand() % next_size + left_most_row_index;
	int col_breach2 = rand() % next_size + left_most_row_index + next_size + 1;
	// passages of the horizontal wall, one on the left of the vertical wall, one on the right of the vertical wall
	int row_breach1 = rand() %  next_size + left_most_col_index;
	int row_breach2 = rand() %  next_size + left_most_col_index + next_size + 1;

	(*maze)[next_size+ left_most_row_index][row_breach1] = FREE_PATH;
	(*critical_nodes)[next_size+ left_most_row_index][row_breach1] = '1';

	(*maze)[next_size+ left_most_row_index][row_breach2] = FREE_PATH;
	(*critical_nodes)[next_size+ left_most_row_index][row_breach2] = '1';

	(*maze)[col_breach1][next_size+ left_most_col_index] = FREE_PATH;
	(*critical_nodes)[col_breach1][next_size+ left_most_col_index] = '1';

	(*maze)[col_breach2][next_size+ left_most_col_index] = FREE_PATH;
	(*critical_nodes)[col_breach2][next_size+ left_most_col_index] = '1';
	printf("recursive(maze, %d,  %d,  %d), breaches [(%d,%d), (%d,%d), (%d,%d), (%d,%d)];\n", next_size , left_most_row_index, left_most_col_index, next_size, row_breach1, next_size, row_breach2, col_breach1, next_size, col_breach2, next_size);

	
	recursive(maze, critical_nodes, next_size - 1  , left_most_row_index, left_most_col_index);
	recursive(maze, critical_nodes, size - next_size - 1, left_most_row_index, left_most_col_index + next_size );
	recursive(maze, critical_nodes, next_size - 1  , left_most_row_index + next_size , left_most_col_index);
	recursive(maze, critical_nodes, size - next_size - 1 , left_most_row_index + next_size , left_most_col_index + next_size );
}


int main() {
	char **maze;
	char **critical_nodes;
	int size;
	printf("Please enter size of maze:");
	if(scanf("%d", &size) != 1) {puts("Illegial!"); exit(1);}
	init(&maze, &critical_nodes, size);
	
	srand(time(NULL));
	int entry_x, entry_y, exit_x, exit_y;
	int y_choice[] = {0, size - 1};
	
	entry_x = rand() % size;
	entry_y = entry_x % (size-1) == 0 ? rand() % size : y_choice[rand()%2];
	do{
		exit_x = rand() % size;
		exit_y = exit_x % (size-1)  == 0 ? rand() % size : y_choice[rand()%2];
	}while(exit_x == entry_x && exit_y == entry_y);

	print_maze(maze, size, NULL);
	puts("########");
	recursive(&maze, &critical_nodes, size - 2, 1, 1);

	maze[entry_x][entry_y] = 'x';
	maze[exit_x][exit_y] = FREE_PATH;
	int *connection_check_entry = available(maze, size - 1, entry_x, entry_y);
	
	// just in case of the entry and exit being blocked
	if(connection_check_entry[0] < 1) {
		
		if(connection_check_entry[1] && 0b1 == 0b1) {
			maze[entry_x][entry_y + 1] = FREE_PATH;
		}else if(connection_check_entry[1] && 0b10 == 0b10) {
			maze[entry_x][entry_y - 1] = FREE_PATH;
		}else if(connection_check_entry[1] && 0b100 == 0b100){
			maze[entry_x + 1][entry_y] = FREE_PATH;
		}else if(connection_check_entry[1] && 0b1000 == 0b1000) {
			maze[entry_x - 1][entry_y] = FREE_PATH;
		}
		
	}

	int *connection_check_exit = available(maze, size - 1, exit_x, exit_y);
	if(connection_check_entry[0] < 1) {
		
		if(connection_check_exit[1] && 0b1 == 0b1) {
			maze[exit_x][exit_y + 1] = FREE_PATH;
		}else if(connection_check_exit[1] && 0b10 == 0b10) {
			maze[exit_x][exit_y - 1] = FREE_PATH;
		}else if(connection_check_exit[1] && 0b100 == 0b100){
			maze[exit_x + 1][exit_y] = FREE_PATH;
		}else if(connection_check_exit[1] && 0b1000 == 0b1000) {
			maze[exit_x - 1][exit_y] = FREE_PATH;
		}
		
	}

	puts("########");

	FILE *outputfilePointer = NULL;
	outputfilePointer = fopen("maze.input", "w");
	if(outputfilePointer == NULL) {
        fprintf(stderr, "Can't open output file !\n");
        exit(1);
    }

    print_maze(maze, size, outputfilePointer);
    puts("Maze generated in file maze.input.");

    destory_2d_arr(&maze, size);
    destory_2d_arr(&critical_nodes, size);

	return 0;
}


/*
a brutal way to generalize a maze
******************


enum Direction { RIGHT, LEFT, UP, DOWN };
const char BUILDING_MATERIAL[] = {
	WALL, FREE_PATH
};

void rand_maze(char ***maze, int size, int entry_x, int entry_y, int exit_x, int exit_y) {
	int size_minus1 = size - 1;
	for(int i = size_minus1; i != -1; i--) {
		(*maze)[0][i] = WALL;
		(*maze)[size_minus1][i] = WALL;
		(*maze)[i][size_minus1] = WALL;
		(*maze)[i][0] = WALL;	
	}
	(*maze)[entry_x][entry_y] = FREE_PATH;
	(*maze)[exit_x][exit_y] = FREE_PATH;
	for(int i = 1; i != size_minus1; i++) {
		for(int j = 1; j != size_minus1; j++) {
			(*maze)[i][j] = BUILDING_MATERIAL[rand() % 2];
		}
	}
}

int available(enum Direction a, char **maze, int mazeSize_minus1, int cx, int cy) {
    switch(a) {
        case RIGHT:{
            if(cy < mazeSize_minus1 && maze[cx][cy + 1] == FREE_PATH) {return 0;}
            return 1;
        }
        case LEFT:{
            if(cy > 0 && maze[cx][cy - 1] == FREE_PATH) {return 0;}
            return 1;
        }
        case UP:{
            if(cx > 0 && maze[cx - 1][cy] == FREE_PATH) {return 0;}
            return 1;
        }
        case DOWN:{
            if(cx < mazeSize_minus1 && maze[cx + 1][cy] == FREE_PATH) {return 0;}
            return 1;
        }
    }
}

int mazeSolve(char ***maze, int mazeSize_minus1, int curr_x, int curr_y, int dstx, int dsty) {

    (*maze)[curr_x][curr_y] = OCCUPIED;
    // mark the current location so that we won't pick it until there is no way to go 

    if(curr_x == dstx && curr_y == dsty) {
    	(*maze)[curr_x][curr_y] = FREE_PATH;
        return 0;
    }

    int up_available = available(UP, *maze, mazeSize_minus1, curr_x, curr_y);
    int down_available = available(DOWN, *maze, mazeSize_minus1, curr_x, curr_y);
    int left_available = available(LEFT, *maze, mazeSize_minus1, curr_x, curr_y);
    int right_available = available(RIGHT, *maze, mazeSize_minus1, curr_x, curr_y);

    if(right_available == 0 && mazeSolve(maze, mazeSize_minus1, curr_x, curr_y + 1, dstx, dsty) == 0) {
    	(*maze)[curr_x][curr_y] = FREE_PATH;
        return 0;
    }
    if(up_available == 0 && mazeSolve(maze, mazeSize_minus1, curr_x - 1, curr_y, dstx, dsty) == 0) {
    	(*maze)[curr_x][curr_y] = FREE_PATH;
        return 0;
    }
    if(left_available == 0 && mazeSolve(maze, mazeSize_minus1, curr_x, curr_y - 1, dstx, dsty) == 0) {
    	(*maze)[curr_x][curr_y] = FREE_PATH;
        return 0;
    }
    if(down_available == 0 && mazeSolve(maze, mazeSize_minus1, curr_x + 1, curr_y, dstx, dsty) == 0) {
    	(*maze)[curr_x][curr_y] = FREE_PATH;
        return 0;
    }
    
    (*maze)[curr_x][curr_y] = FREE_PATH;
    // It is a dead end, so we need to go back now

    return 1;
}

void brutal_way() {

	int size;
	if(scanf("%d", &size) != 1 || size < 10 || size > 30){puts("Illegial number."); exit(1);}

	char **maze;
	init(&maze, size);
	srand(time(NULL));
	int entry_x, entry_y, exit_x, exit_y, succeed = 1;
	
	entry_x = rand() % size;
	entry_y = entry_x == 0 ? rand() % size : 0;
	do{
		exit_x = (rand()) % size;
		exit_y = exit_x == 0 ? (rand()) % size : 0;
	}while(exit_x == entry_x && exit_y == entry_y);

	printf("Entry at (%d, %d)\n", entry_x, entry_y);
	printf("Exit at (%d, %d)\n", exit_x, exit_y);
	sleep(1);

	while(mazeSolve(&maze, size - 1, entry_x, entry_y, exit_x, exit_y) == 1) {
		puts("####");
		print_maze(maze, size);
		rand_maze(&maze, size, entry_x, entry_y, exit_x, exit_y);
	}
	maze[entry_x][entry_y] = 'x';
	print_maze(maze, size);

}

*/
