#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <stdbool.h>

enum Direction { RIGHT, LEFT, UP, DOWN }; //might be useful

// a simple stack, since I am using recursion, output will in reverse order
int stack[1000];
int *sp;
#define push(sp, n) (*((sp)++) = (n))
#define pop(sp) (*--(sp))
void print_stack();

const char *OUTPUT[] = {
    "RIGHT", "LEFT", "UP", "DOWN"
};
const char OCCUPIED = '*'; // we was here
const char FREE_PATH = '.'; // unexplorered area
const char DEAD_END = '@';

void destory_maze(char ***maze, int size);
void get_maze(FILE *f, int size, char ***dst, int *etx, int *ety, int *exx, int *exy); // read input, load it into RAM
int available(enum Direction a, char **maze, int mazeSize_minus1, int cx, int cy);// check which direction is availiable
int mazeSolve(char ***maze, int mazeSize_minus1, int curr_x, int curr_y, int dstx, int dsty);// a recursive method solving mazes

int main( int argc, const char* argv[] )
{
    sp = stack;// initialize stack
    push(sp, 0); // stack[0] is the size of the current stack

    if( argc < 2 ) { 
        //checks for the input file name
        printf( "error; no input file name\n" );
        return 1;
    }

    FILE *inputfilePointer, *outputfilePointer = NULL;
    inputfilePointer = fopen( argv[1], "r" );
    if(inputfilePointer == NULL) {
        fprintf(stderr, "Can't open output file %s!\n",argv[1]);
        exit(1);
    }
    if(argc == 3) {
        outputfilePointer = fopen(argv[2], "w");
        if(outputfilePointer == NULL) {
            fprintf(stderr, "Can't open output file %s!\n",argv[2]);
            exit(1);
        }
    }

    int numberOfTestCases = 0;
    fscanf( inputfilePointer, "%d\n", &numberOfTestCases );

    for( int testCaseNumber = 0; testCaseNumber < numberOfTestCases; testCaseNumber++ ) {
        int mazeSize;
        char **maze;

        int entry_x, entry_y;
        int exit_x, exit_y;
        fscanf( inputfilePointer, "%d\n", &mazeSize );
        
        printf( "ENTER\n" );
        if(argc == 3) {fprintf(outputfilePointer, "ENTER\n");}
        get_maze(inputfilePointer, mazeSize, &maze, &entry_x, &entry_y, &exit_x, &exit_y);
        if (mazeSolve(&maze, mazeSize - 1, entry_x, entry_y, exit_x, exit_y) == 0) {
            print_stack(outputfilePointer);
        }
        printf( "EXIT\n***\n" );
        if(argc == 3) {fprintf(outputfilePointer, "EXIT\n***\n");}
        destory_maze(&maze, mazeSize);       
    }

    fclose(inputfilePointer);
    fclose(outputfilePointer);
    return 0;
}

void print_stack(FILE *fp) {
    int tmp;
    while(stack[0] != 0) {
        tmp = pop(sp);
        (stack[0])--;
        printf("%s\n", OUTPUT[tmp]);
        if(fp != NULL) {
            fprintf(fp, "%s\n", OUTPUT[tmp]);
        }
    }
}

void destory_maze(char ***maze, int size) {
    for(int i = size - 1; i != -1; i--) {
        free((*maze)[i]);
    }
    free((*maze));
}

void get_maze(FILE *f, int size, char ***dst, int *etx, int *ety, int *exx, int *exy) {

    (*dst) = (char **)malloc(size * sizeof(char *));
    int arr_max_index = size - 1;

    for(int i = 0; i != size; i++) {
        (*dst)[i] = malloc(size); 
        fscanf(f, "%s\n", (*dst)[i]);
        // Find the entry
        char *entry_ptr = strchr((*dst)[i], 'x');
        if (entry_ptr) {
            (*etx) = i;
            (*ety) = entry_ptr - (*dst)[i];
            printf("Entry point is at (%d, %d)\n", (*etx), (*ety));
        }
        // Find the exit
        if ((i == 0 || i == arr_max_index)) {
            char *exit_ptr = strchr((*dst)[i], '.');
            if (exit_ptr) {
                (*exx) = i;
                (*exy) = exit_ptr - (*dst)[i];
                printf("Exit point is at (%d, %d)\n", (*exx), (*exy));
            }
        } else if ((*dst)[i][0] == '.') {
            (*exx) = i;
            (*exy) = 0;
            printf("Exit point is at (%d, %d)\n", (*exx), (*exy));
        } else if ((*dst)[i][arr_max_index] == '.') {
            (*exx) = i;
            (*exy) = arr_max_index;
            printf("Exit point is at (%d, %d)\n", (*exx), (*exy));
        }
    }
}

int available(enum Direction a, char **maze, int mazeSize_minus1, int cx, int cy) {
    switch(a) {
        case RIGHT:{
            if(cy < mazeSize_minus1 && maze[cx][cy + 1] == FREE_PATH) {return 0;}
            return 1;
        }
        case LEFT:{
            if(cy > 0 && maze[cx][cy - 1] == FREE_PATH) {return 0;}
            return 1;
        }
        case UP:{
            if(cx > 0 && maze[cx - 1][cy] == FREE_PATH) {return 0;}
            return 1;
        }
        case DOWN:{
            if(cx < mazeSize_minus1 && maze[cx + 1][cy] == FREE_PATH) {return 0;}
            return 1;
        }
    }
}

int mazeSolve(char ***maze, int mazeSize_minus1, int curr_x, int curr_y, int dstx, int dsty) {

    (*maze)[curr_x][curr_y] = OCCUPIED;
    // mark the current location so that we won't pick it until there is no way to go 

    if(curr_x == dstx && curr_y == dsty) {
        return 0;
    }

    int up_available = available(UP, *maze, mazeSize_minus1, curr_x, curr_y);
    int down_available = available(DOWN, *maze, mazeSize_minus1, curr_x, curr_y);
    int left_available = available(LEFT, *maze, mazeSize_minus1, curr_x, curr_y);
    int right_available = available(RIGHT, *maze, mazeSize_minus1, curr_x, curr_y);

    if(right_available == 0 && mazeSolve(maze, mazeSize_minus1, curr_x, curr_y + 1, dstx, dsty) == 0) {
        push(sp, RIGHT);
        (stack[0])++;
        return 0;
    }
    if(up_available == 0 && mazeSolve(maze, mazeSize_minus1, curr_x - 1, curr_y, dstx, dsty) == 0) {
        push(sp, UP); 
        (stack[0])++;
        return 0;
    }
    if(left_available == 0 && mazeSolve(maze, mazeSize_minus1, curr_x, curr_y - 1, dstx, dsty) == 0) {
        push(sp, LEFT);
        (stack[0])++;
        return 0;
    }
    if(down_available == 0 && mazeSolve(maze, mazeSize_minus1, curr_x + 1, curr_y, dstx, dsty) == 0) {
        push(sp, DOWN);
        (stack[0])++;
        return 0;
    }
    
    (*maze)[curr_x][curr_y] = DEAD_END;
    // It is a dead end, so we need to go back now

    return 1;
}