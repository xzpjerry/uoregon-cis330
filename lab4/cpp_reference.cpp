#include <iostream>
using namespace std;
void increment1(int x, int y) {
    cout << "Enter increment1, they should be\t";
    cout << ++x << "\t" << ++y << endl;
}
void increment2(/*const*/ int &x, /*const*/ int &y) {
    cout << "Enter increment2, they should be\t";
    cout << ++x << "\t" << ++y << endl;
}
void increment3(int *x, int *y) {
    cout << "Enter increment3, they should be\t";
    cout << ++(*x) << "\t" << ++(*y) << endl;
}

int main() {
    int x1 = 0, y1 = 1;
    increment1(x1, y1);
    cout << "increment1 finished, x1:" << x1 << "\ty1: " << y1 << endl;
    cout << "####" << endl;

    int x2 = 0, y2 = 1;
    increment2(x2 , y2);
    cout << "increment2 finished, x2:" << x2 << "\ty2: " << y2 << endl;
    cout << "####" << endl;

    int x3 = 0, y3 = 1;
    increment3(&x3 , &y3);
    cout << "increment3 finished, x3:" << x3 << "\ty3: " << y3 << endl;

    return 0;
}