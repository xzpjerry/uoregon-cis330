#include<stdio.h>
#include<stdlib.h>
#include <time.h>

const char rps[] = {'r', 'p', 's'};
struct rps
{
	char id;

	void (*compare_to)(struct rps*, struct rps*);
};
void __comparison__(struct rps* self, struct rps* b) {
	char ca = self -> id;
	char cb = b -> id;
	switch (ca) {
		case 'r':
			{
				if(cb == 'p') {
					puts("Paper wins.");
				} else if (cb == 's'){
					puts("Rock wins.");
				} else {
					puts("This game is a tie.");
				}
                break;
			};
        case 's':
        {
            if(cb == 'r') {
                puts("Rock wins.");
            } else if (cb == 'p'){
                puts("Sucissors wins.");
            } else {
                puts("This game is a tie.");
            }
            break;
        };
        case 'p':
        {
            if(cb == 's') {
                puts("Sucissors wins.");
            } else if (cb == 'r'){
                puts("Paper wins.");
            } else {
                puts("This game is a tie.");
            }
            break;
        };
		default: {puts("Invalid user choice, you must enter rock, paper, or scissors.");}
	}
}

int validation(char ch) {
    for(int i = 2; i != -1; i--){
        if (rps[i] == ch) {
            return 0;
        }
    }
    return 1;
}

int main(int argc, char const *argv[])
{
    srand(time(NULL));

	struct rps player;
    player.compare_to = __comparison__;
    puts("Enter 'r' for rock, 'p' for paper, or 's' for scissors:");
    scanf("%c", &player.id);
    if (validation(player.id) != 0) {
        puts("Invalid user choice, you must enter 'r' for rock, 'p' for paper, or 's' for scissors.");
        goto END;
    }

    printf("You picked %c\n", player.id);
    struct rps computer;
    computer.id = rps[rand() % 3];
    printf("Computer picked %c\n", computer.id);
    player.compare_to(&player, &computer);

    END:
	return 0;
}