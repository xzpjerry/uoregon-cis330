#include <iostream>
#include <vector>
using namespace std;

class Pizza
{
public:
    Pizza() {
        length = 1;
        v = new char*[1];
        v[0] = new char[1];
        v[0][0] = 'A';
    }
    Pizza(const char *a[], int length) {
        this -> length = length;
        v = new char*[length];
        for(int i = 0; i != length; i++) {
            v[i] = new char[sizeof(a[i])];
            strcpy(v[i], a[i]);
        }
    }
    ~Pizza(){
        for(int i = 0; i != this->length; i++) {
            delete v[i];
        }
        delete v;
    }
    void print() {
        for(int i = 0; i != this->length; i++) {
            cout << v[i] << endl;
        }
    }

    // // overload operator
     friend ostream &operator<<(ostream &output, const Pizza &A) {
        for(int i = 0; i != A.length; i++) {
            output << A.v[i] << ' ';
        }
        output << endl;
        return output;
    }
//     friend istream &operator>>(istream &input, Pizza &A) {
//         char **newarr = new char*[A.length + 1];
//         for(int i = 0; i != A.length; i++) {
//             newarr[i] = new char[sizeof(A.v[i])];
//             memcpy(newarr[i], A.v[i], sizeof(A.v[i]));
//             delete A.v[i];
//         }
//         A.length++;
//         delete A.v;
//         A.v = newarr;
//         input >> A.v[A.length - 1];
//         return input;
//     }

private:
    char **v;
    int length;
};

int main() {
//    vector<int> v;
//    for(int i = 1; i != 9; i++)
//        v.push_back(i);
//
//
//    int counter = 0;
//    for(vector<int>::iterator it = v.begin();  it != v.end(); it++) {
//        cout << *it << " ";
//    }
//    cout << "\n#######" << endl;
//
//
//    counter = 0;
//    for(vector<int>::reverse_iterator rit = v.rbegin();  rit != v.rend(); rit++) {
//        cout << *rit << " ";
//    }
//    cout << endl;
    const char *toppings[2] = {"TopA", "TopB"};
    Pizza a;
    cout << a;
    const char *test = "TopA";
    Pizza b(toppings, 2);
    cout << b;

    return 0;
}