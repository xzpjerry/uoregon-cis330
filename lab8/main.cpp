#include <iostream>
#include <string>
using namespace std;


class SimpleString {
public:
    SimpleString(): input("default") {}
    SimpleString(std::string str) : input(str) {}
    void setString(std::string str) {input = str;}
    std::string getString() {return input;}

    // All operator overloading member functions.
    SimpleString& operator=(const SimpleString &new_value) {
        input = new_value.input;
    }
    SimpleString operator+(const SimpleString &append_with) {
        input += append_with.input;
    }

    // Pre-increment and pre-decrement.
    SimpleString& operator++() {
        input += "*";
    }
    SimpleString& operator--() {
        input.pop_back();
    }

    // Operator overloading friend functions.
    friend std::ostream& operator<<(std::ostream& os, const SimpleString& s);
    friend std::istream& operator>>(std::istream& is, SimpleString& s);

private:
    std::string input;
};

std::ostream& operator<<(std::ostream& os, const SimpleString& s) {
    os << s.input;
}
std::istream& operator>>(std::istream& is, SimpleString& s) {
    is >> s.input;
}




int main() {
    SimpleString test;
    cout << test;

    cin >> test;
    cout << test;
    return 0;
}