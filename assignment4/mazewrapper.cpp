extern "C" {
	#include "libmaze.h"
}

int main(int argc, const char *argv[]) {
	sp = stack;// initialize stack
    push(sp, 0); // stack[0] is the size of the current stack

    if( argc < 2 ) { 
        //checks for the input file name
        printf( "error; no input file name\n" );
        exit(1);
    }

    FILE *inputfilePointer, *outputfilePointer = NULL;
    inputfilePointer = fopen( argv[1], "r" );
    if(inputfilePointer == NULL) {
        fprintf(stderr, "Can't open output file %s!\n",argv[1]);
        exit(1);
    }
    if(argc == 3) {
        outputfilePointer = fopen(argv[2], "w");
        if(outputfilePointer == NULL) {
            fprintf(stderr, "Can't open output file %s!\n",argv[2]);
            exit(1);
        }
    }

	run(inputfilePointer, outputfilePointer);
	return 0;
}