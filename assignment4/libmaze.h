
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <stdbool.h>
enum Direction { RIGHT, LEFT, UP, DOWN }; //might be useful

// a simple stack, since I am using recursion, output will in reverse order
int stack[1000];
int *sp;
#define push(sp, n) (*((sp)++) = (n))
#define pop(sp) (*--(sp))

const char *OUTPUT[] = {
    "RIGHT", "LEFT", "UP", "DOWN"
};
const char OCCUPIED = '*'; // we was here
const char FREE_PATH = '.'; // unexplorered area
const char DEAD_END = '@';



void destory_maze(char ***maze, int size);
void get_maze(FILE *f, int size, char ***dst, int *etx, int *ety, int *exx, int *exy); // read input, load it into RAM
int available(enum Direction a, char **maze, int mazeSize_minus1, int cx, int cy);// check which direction is availiable
int mazeSolve(char ***maze, int mazeSize_minus1, int curr_x, int curr_y, int dstx, int dsty);// a recursive method solving mazes
void run( FILE *inputfilePointer, FILE *outputfilePointer );
void print_stack(FILE *fp);

