#include "maze.hpp"
using namespace std;

int main(int argc, char *argv[]) {
	if(argc != 3 ) {
		cout << "No enough arguments, 3 needed, \"this program, input, output\"" << endl;
		return 1;
	}

	ifstream myfile(argv[1]); // input
	string first_line; // a buffer
	getline(myfile, first_line); // get number of mazes
	int cases = stoi(first_line);

	ofstream target(argv[2]); // output
 
	for(int i = cases; i != 0; i--) {
		getline(myfile, first_line); // get every maze's size
		//cout << first_line << endl;
		int size = stoi(first_line);

		Maze a(size);
		a.readFromFile(myfile);
		target << "Enter" << endl;
		while(a.atExit() != true) {
			// a.print_maze();
			// sleep(1);
			a.step();
		}
		
		target << a.output();
		cout << "##########Done##########" << endl;
		target << "##########Done##########" << endl;
	}
	myfile.close();
	target.close();
	return 0;
}