/*
 * maze.hpp
 *
 *  Created on: Jan 27, 2014
 *      Author: norris
 */

#ifndef MAZE_HPP_
#define MAZE_HPP_

#include <fstream>
#include <string>
#include <numeric> // + operator for string
#include <iostream>


class Maze
{
public:
	Maze(int size);
	~Maze();

	enum Direction { DOWN, RIGHT, UP, LEFT };

	// Implement the following functions:

	// read maze from file, find starting location
	void readFromFile(std::ifstream &f);

	// make a single step advancing toward the exit
	void step();

	// return true if the maze exit has been reached, false otherwise
	bool atExit();

	// set row and col to current position of 'x'
	void getCurrentPosition(int &row, int &col);

	// You can add more functions if you like
	bool available(Direction a);
	void print_maze();
	std::string output();
private:
	void moveTo(Direction a);
	void backTo(Direction a);
	void push(Direction a);
	Direction pop();

	// Private data
	int size, xStart, yStart, xEnd, yEnd, row, col, stack_size;; // size and position information
	char **mazeData; // where the maze will be stored
	Direction *currentDirection;  // direction information
	Direction *stack;
	std::string output_result;
};


#endif /* MAZE_HPP_ */
