#include "maze.hpp"
//#include <cstddef> // size_t
//#include <random> saved for maze_gen, if we are going to do so
// typedef std::mt19937 MyRNG;
// random_device rd;
// 	MyRNG rng(rd());
// 	uniform_int_distribution<uint32_t> uint_dist(0,size - 1);
// 		xStart = uint_dist(rng);
// 		yStart = uint_dist(rng);
using namespace std;

Maze::Maze(int size) {
	this -> size = size;
	stack = new Direction[1000];
	currentDirection = stack;
	stack_size = 0;
	mazeData = new char*[size];
	for(int i = size - 1; i != -1; i--) {
		mazeData[i] = new char[size];
	}
}
Maze::~Maze() {
	delete stack;
	for(int i = size - 1; i != -1; i--) {
		delete mazeData[i];
	}
	delete mazeData;  
}
void Maze::readFromFile(ifstream &f) {
	string line;
	int col_counter, row_counter = 0;
	if(f.is_open()) {
		while(row_counter != size) {
			getline(f, line);
			int cx = row_counter;
			col_counter = 0;
			for(int i = 0; i < (int)line.length(); i++) {
				char tmp = line[i];
				if(tmp == '@') {
					mazeData[cx][i] = '0';
					col_counter++;
				} else if(tmp == '.') {
					mazeData[cx][i] = '1';
					if(cx == 0 || cx == size - 1 || i == 0 || i == size - 1) { // get exit
						xEnd = cx;
						yEnd = i;
					}
					col_counter++;
				} else if(tmp == 'x') { // get entry
					mazeData[cx][i] = '1';
					xStart = cx;
					yStart = i;
					col_counter++;
				}
				//cout << mazeData[cx][i];
			}
			//cout << endl;
			if(col_counter == size) {row_counter++;}
		}
		
		row = xStart;
		col = yStart;
		//cout << "Init currentDirection ";
		if(available(RIGHT)) {push(RIGHT);}
		else if(available(LEFT)) {push(LEFT);}
		else if(available(UP)) {push(UP);}
		else if(available(DOWN)){push(DOWN);}
		else { cout << "Incorrect format" << endl; exit(1);}

	} else {
		cout << "Unable to open this file" << endl;
		exit(1);
	}
}
void Maze::step() {
	mazeData[row][col] = 'x'; // Marked we were here
	switch(*currentDirection) {
		case DOWN: {
			//cout << "Current @" << "DOWN" << endl;
			if(available(LEFT)) {moveTo(LEFT);}
			else if(available(DOWN)) {moveTo(DOWN);}
			else if(available(RIGHT)) {moveTo(RIGHT);}
			else {backTo(UP);}// dead end, mark it as -1 and go back
			break;
		}
		case UP: {
			//cout << "Current @" << "UP" << endl;
			if(available(RIGHT)) {moveTo(RIGHT);}
			else if(available(UP)) {moveTo(UP);}
			else if(available(LEFT)) {moveTo(LEFT);}
			else {backTo(DOWN);}
			break;
		}
		case LEFT: {
			//cout << "Current @" << "LEFT" << endl;
			if(available(UP)) {moveTo(UP);}
			else if(available(LEFT)) {moveTo(LEFT);}
			else if(available(DOWN)) {moveTo(DOWN);}
			else {backTo(RIGHT);}
			break;
		}
		case RIGHT: {
			//cout << "Current @" << "RIGHT" << endl;
			if(available(DOWN)) {moveTo(DOWN);}
			else if(available(RIGHT)) {moveTo(RIGHT);}
			else if(available(UP)) {moveTo(UP);}
			else {backTo(LEFT);}
			break;
		}
	}
}

bool Maze::atExit() {
	if(row == xEnd && col == yEnd) {return true;}
	return false;
}
void Maze::getCurrentPosition(int &row, int &col) {
	row = this -> row;
	col = this -> col;
}

bool Maze::available(Direction a) { // reuse maze.c's code, a judge for current surrending
	int mazeSize_minus1 = size - 1;
	int x = row, y = col;
	switch(a) {
        case RIGHT:{
            if(y == mazeSize_minus1 || mazeData[x][y + 1] != '1')
            	return false; // cannot go right
            break;
        }
        case LEFT:{
            if(y == 0 || mazeData[x][y - 1] != '1')
            	return false;
            break;
        }
        case UP:{
            if(x == 0 || mazeData[x - 1][y] != '1')
            	return false;
            break;
        }
        case DOWN:{
            if(x == mazeSize_minus1 || mazeData[x + 1][y] != '1')
            	return false;
            break;
        }
    }
    return true;
}

void Maze::print_maze() {
	for(int i = 0; i != size; i++) {
		for(int j = 0; j != size; j++)
			cout << mazeData[i][j] << ' ';
		cout << endl;
	}
}

string Maze::output() {
	return output_result;
}

void Maze::moveTo(Maze::Direction a) {
	switch(a) {
		case RIGHT: {
			cout << "RIGHT" << endl; 
			output_result += "RIGHT\n";
			col++; 
			push(RIGHT); // keep current direction in record so that 
			break;	// we could recover it when we need to go back
		}
		case LEFT: {
			cout << "LEFT" << endl; 
			output_result += "LEFT\n";
			col--; 
			push(LEFT);
			break;
		}
		case UP: {
			cout << "UP" << endl; 
			output_result += "UP\n";
			row--; 
			push(UP);
			break;
		}
		case DOWN: {
			cout << "DOWN" << endl; 
			output_result += "DOWN\n";
			row++; 
			push(DOWN);
			break;
		}
	}
}
void Maze::backTo(Maze::Direction a) {
	switch(a) {
		case RIGHT: {
			mazeData[row][col] = '3'; // mark here is a dead end
			cout << "RIGHT" << endl;
			output_result += "RIGHT\n";
			col++;
			mazeData[row][col] = '1';
			pop(); // recover the dirction when we were at the previous [row][col]
			break;
		}
		case LEFT: {
			mazeData[row][col] = '3';
			cout << "LEFT" << endl;
			output_result += "LEFT\n";
			col--;
			mazeData[row][col] = '1';
			pop();
			break;
		}
		case UP: {
			mazeData[row][col] = '3'; 
			cout << "UP" << endl;
			output_result += "UP\n";
			row--;
			mazeData[row][col] = '1';
			pop();
			break;
		}
		case DOWN: {
			mazeData[row][col] = '3';
			cout << "DOWN" << endl;
			output_result += "DOWN\n";
			row++;
			mazeData[row][col] = '1';
			pop();
			break;
		}
	}
}
// a simple stack implmentation
void Maze::push(Maze::Direction a) {
	stack_size++;
	currentDirection++;
	*(currentDirection) = a;
}
Maze::Direction Maze::pop() {
	stack_size--;
	currentDirection--;
	return *currentDirection;
}