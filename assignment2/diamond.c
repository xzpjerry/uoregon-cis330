#include <stdio.h>
#include "square.h"

/* Print a diamond containing the digits 0 - 9 */
void printNumberDiamond(const int size, int **square) {
	int i = 1;
	for(; i <= size; i += 2) {
		int diff = size - i;
        for(int j = diff; j != -1; j--) {
            printf(" ");
        }
		for(int j = 0; j != i; j++) {
			printf("%d ", square[0][j]);
		}
		puts("");
	}

	i -= 4;
	for(; i >= 1; i -= 2) {
		int diff = size - i;
        for(int j = diff; j != -1; j--) {
            printf(" ");
        }
		for(int j = 0; j != i; j++) {
			printf("%d ", square[0][j]);
		}
		puts("");
	}
}
