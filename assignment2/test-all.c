#include <stdio.h> 
#include "square.h"
#include "triangle.h"
#include "diamond.h"

int main(int argc, char const *argv[])
{

	int size;
	int height;
	int **arr;

	/*
	*
	*/
	puts("Problem 1:");
	while(1){
		printf("Please enter the size of the square [2-10]:");
		fseek(stdin,0,SEEK_END);
		if(scanf("%d", &size) == 1) {
			if(size >= 2 && size <= 10) {
				break;
			}
		}
	}
	
    allocateNumberSquare(size, &arr);
    
    initializeNumberSquare(size, arr);
    printNumberSquare(size, arr);
    
    deallocateNumberSquare(size, &arr);
    
   	/*
   	 *
     */
    puts("Problem 2:");
    
    while(1) {
        printf("Please enter the height of the triangle [1-5]:");
        fseek(stdin,0,SEEK_END);
        if(scanf("%d", &height) == 1) {
            if(height >= 1 && height <= 5) {
                break;
            }
        }
    }
    allocateNumberTriangle(height, &arr);
    
    initializeNumberTriangle(height, arr);
    printNumberTriangle(height, arr);
    
    deallocateNumberTriangle(height, &arr);

    /*
    *
    */
    puts("Problem 3:");
    
    while(1) {
        printf("Please enter the size of the diamond [an odd number between 3 and 9 (inclusive)]:");
        fseek(stdin,0,SEEK_END);
        if(scanf("%d", &height) == 1) {
            if(height >= 3 && height <= 9) {
                if((height & 0b01) != 0) {
                    break;
                }
            }
        }
    }
    allocateNumberSquare(height, &arr);
    initializeNumberSquare(height, arr);
    printNumberDiamond(height, arr);
    deallocateNumberSquare(height, &arr);



    return 0;
}
