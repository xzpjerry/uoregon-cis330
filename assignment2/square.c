#include<stdio.h>
#include<stdlib.h>
#include<string.h>

int *sample_array_for_initializeNumberSquare;
/* Allocate a square of size "size" (a 2-D array of int) */
// allocateNumberSquare(10, &(arr)); arr : int **
void allocateNumberSquare(const int size, int ***square) {
    (*square) = (int **)malloc(size * sizeof(int *));
    sample_array_for_initializeNumberSquare = malloc(size * sizeof(int));
    for(int i = size - 1; i != -1; i--) {
        (*square)[i] = (int *)malloc(size * sizeof(int));
        sample_array_for_initializeNumberSquare[i] = i;
    }
}

/* Initialize the 2-D square array */
// for every row in arr
// initializeNumberSquare(COL, arr[row])

void initializeNumberSquare(const int size, int **square) {
    for(int i = 0; i != size; i++) {
        memcpy(square[i], sample_array_for_initializeNumberSquare, size * sizeof(int));
    }
}

/* Print a formatted square */
void printNumberSquare(const int size, int **square) {
    for(int i = 0; i != size; i++) {
        for(int j = 0; j != size; j++) {
            printf("%d ", square[i][j]);    
        }
        puts("");
    }
}

/* Free the memory for the 2-D square array */
void deallocateNumberSquare(const int size, int ***square) {
    for(int i = size - 1; i != -1; i--) {
        free((*square)[i]);
    }
    free(*square);
    free(sample_array_for_initializeNumberSquare);
}

