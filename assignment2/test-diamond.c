#include <stdio.h>
#include "square.h"
#include "diamond.h"

int main() {
    puts("Problem 3:");
    int height;
    while(1) {
        printf("Please enter the size of the diamond [an odd number between 3 and 9 (inclusive)]:");
        fseek(stdin,0,SEEK_END);
        if(scanf("%d", &height) == 1) {
            if(height >= 3 && height <= 9) {
                if((height & 0b01) != 0) {
                    break;
                }
            }
        }
    }
    int **arr;
    allocateNumberSquare(height, &arr);
    initializeNumberSquare(height, arr);
    printNumberDiamond(height, arr);
    deallocateNumberSquare(height, &arr);



    return 0;
}
