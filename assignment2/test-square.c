#include <stdio.h> 
#include "square.h"

int main(int argc, char const *argv[])
{
	puts("Problem 1:");
	int size;
	while(1){
		printf("Please enter the size of the square [2-10]:");
		fseek(stdin,0,SEEK_END);
		if(scanf("%d", &size) == 1) {
			if(size >= 2 && size <= 10) {
				break;
			}
		}
	}
	
    int **arr;
    allocateNumberSquare(size, &arr);
    
    initializeNumberSquare(size, arr);
    printNumberSquare(size, arr);
    
    deallocateNumberSquare(size, &arr);
    return 0;
}
