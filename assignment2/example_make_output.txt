gcc -std=c11 -g -Wall   -c -o test-square.o test-square.c
gcc -c -std=c11 -g -Wall square.c
gcc -o test-square.exe test-square.o square.o -lm
gcc -std=c11 -g -Wall   -c -o test-triangle.o test-triangle.c
gcc -c -std=c11 -g -Wall triangle.c
gcc -o test-triangle.exe test-triangle.o triangle.o -lm
gcc -std=c11 -g -Wall   -c -o test-diamond.o test-diamond.c
gcc -c -std=c11 -g -Wall diamond.c
gcc -o test-diamond.exe test-diamond.o diamond.o square.o -lm
gcc -std=c11 -g -Wall   -c -o test.o test.c
gcc -o test-test.exe test.o square.o triangle.o diamond.o -lm
