#include <stdio.h>
#include "triangle.h"

int main() {
    puts("Problem 2:");
    int height;
    while(1) {
        printf("Please enter the height of the triangle [1-5]:");
        fseek(stdin,0,SEEK_END);
        if(scanf("%d", &height) == 1) {
            if(height >= 1 && height <= 5) {
                break;
            }
        }
    }
    int **arr;
    allocateNumberTriangle(height, &arr);
    
    initializeNumberTriangle(height, arr);
    printNumberTriangle(height, arr);
    
    deallocateNumberTriangle(height, &arr);
    return 0;
}
