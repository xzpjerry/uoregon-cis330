#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int *sample_array_for_initializeNumberTriangle;

/* Allocate a triangle of height "height" (a 2-D array of int) */
void allocateNumberTriangle(const int height, int ***triangle) {
    (*triangle) = (int **)malloc(height * sizeof(int *));
    int longest_row_size = 2 * height - 1;
    sample_array_for_initializeNumberTriangle = malloc((longest_row_size) * sizeof(int));

    for(int i = height; i != 0; i--) {
        int current_index = i - 1;
        (*triangle)[current_index] = (int *)malloc((current_index + i) * sizeof(int));
        sample_array_for_initializeNumberTriangle[current_index] = current_index;
    }
    for(int i = height; i != longest_row_size; i++) {
        sample_array_for_initializeNumberTriangle[i] = i;
    }

}

/* Initialize the 2-D triangle array */
void initializeNumberTriangle(const int height, int **triangle) {
    for(int i = height; i != 0; i--) {
        int this_row_size = (2 * i - 1) * sizeof(int);
        memcpy(triangle[i - 1], sample_array_for_initializeNumberTriangle, this_row_size);
    }
}

/* Print a formatted triangle */
void printNumberTriangle(const int size, int **triangle) {
    for(int i = 1; i != size + 1; i++) {
        int this_row_size = 2 * i - 1;
        int diff = size - i;
        for(int j = 2 * diff; j != -1; j--) {
            printf(" ");
        }
        for(int j = 0; j != this_row_size; j ++) {
            printf("%d ", triangle[i - 1][j]);
        }
        puts("");
    }
}

/* Free the memory for the 2-D triangle array */
void deallocateNumberTriangle(const int size, int ***triangle) {
    for(int i = size - 1; i != -1; i--) {
        free((*triangle)[i]);
    }
    free((*triangle));
    free(sample_array_for_initializeNumberTriangle);
}

